import { compare, hash } from "bcrypt"
import { IsEmail } from "class-validator"
import {
    AfterInsert, AfterRemove, AfterUpdate,
    BaseEntity,
    BeforeInsert,
    BeforeUpdate,
    Column,
    CreateDateColumn,
    Entity, ManyToOne, OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import Chat from "./Chat";
import Message from "./Message";
import Ride from "./Ride";
import Place from "./Place";

@Entity()
class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "varchar",
        unique: true,
        nullable: true
    }) @IsEmail()
    email: string | null;

    @Column({
        type: "boolean",
        default: false
    })
    verifiedEmail: boolean;

    @Column({ type: "text" })
    firstName: string;

    @Column({ type: "text" })
    lastName: string;

    @Column({ type: "text", nullable: true })
    bio: string;

    @Column({ type: "int", nullable: true })
    age: number;

    @Column({ type: "text", nullable: true })
    pass: string;

    @Column({ type: "text", nullable: true })
    phoneNumber: string;

    @Column({
        type: "boolean",
        default: false
    })
    verifiedPhoneNumber: boolean;

    @Column({ type: "text", nullable: true })
    facebookId: string;

    @Column({
        type: "boolean",
        default: false
    })
    isDriving: boolean;

    @Column({
        type: "boolean",
        default: false
    })
    isRiding: boolean;

    @Column({
        type: "boolean",
        default: false
    })
    isTaken: boolean;

    @Column({
        type: "double precision",
        default: 0
    })
    lastLng: number;

    @Column({
        type: "double precision",
        default: 0
    })
    lastLat: number;

    @Column({
        type: "double precision",
        default: 0
    })
    lastOrientation: number;

    @OneToMany(type => Chat, chat => chat.passenger)
    chatsAsPassenger: Chat[];

    @OneToMany(type => Chat, chat => chat.driver)
    chatsAsDriver: Chat[];

    @OneToMany(type => Message, message => message.user)
    messages: Message[];

    @OneToMany(type => Ride, ride => ride.passenger)
    ridesAsPassenger: Ride[];

    @OneToMany(type => Ride, ride => ride.driver)
    ridesAsDriver: Ride[];

    @OneToMany(type => Place, place => place.user)
    places: Place[];

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string;

    get fullName(): string {
        return `${this.firstName} ${this.lastName}`
    }

    public passwordsComparing(password: string): Promise<boolean> {
        return compare(password, this.pass)
    }

    @BeforeInsert()
    @BeforeUpdate()

    async savePassword(): Promise<void> {
        if(this.pass) {
            this.pass = await this.hashPassword(this.pass);
        }
    }

    private hashPassword(password: string): Promise<string> {
        return hash(password, 10)
    }

    @AfterInsert()
    logAfterInsert() {
        //TODO Implement logging after Insert
        console.log(`User ${this.email} Id ${this.id} - created`);
    }

    @AfterUpdate()
    logAfterUpdate() {
        //TODO Implement logging after Update
        console.log(`User ${this.email} Id ${this.id} - updated`);
    }

    @AfterRemove()
    logAfterRemove() {
        //TODO Implement logging after Remove
        console.log(`User ${this.email} Id ${this.id} - deleted`);
    }
}

export default User;

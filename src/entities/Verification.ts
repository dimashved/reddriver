import {
    AfterInsert, AfterRemove,
    BaseEntity,
    BeforeInsert,
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import { verificationTarget } from "../types/types";

const PHONE: string = "PHONE"
const EMAIL: string = "EMAIL"

@Entity()
class Verification extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "enum", enum: [PHONE, EMAIL] })
    target: verificationTarget;

    @Column({ type: "text" })
    payload: string;

    @Column({ type: "text" })
    code: string;

    @Column({ type: "boolean", default: false })
    verified: boolean;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string;

    @BeforeInsert()
    // @ts-ignore
    private createKey(): void {
        if (process.env.APP_ENV === 'dev') {
            this.code = '12345';
        } else if (process.env.APP_ENV === 'prod') {
            if(this.target === PHONE) {
                this.code = Math.floor(Math.random() * 100000).toString();
            } else if (this.target === EMAIL) {
                this.code = Math.random().toString(36).substr(2)
            }
        }
    }

    @AfterInsert()
    logAfterInsert() {
        //TODO Implement logging after Insert
        console.log(`Code ${this.code} Id ${this.id} - created`);
    }

    @AfterRemove()
    logAfterRemove() {
        //TODO Implement logging after Remove
        console.log(`Code ${this.code} Id ${this.id} - deleted`);
    }
}

export default Verification;

export const typeDefs = ["type EmailSignInResponse {\n  ok: Boolean!\n  error: String\n  token: String\n}\n\ntype Mutation {\n  EmailSignIn(email: String!, pass: String!): EmailSignInResponse!\n  EmailSignUp(firstName: String!, lastName: String!, email: String, pass: String!, age: Int!, phoneNumber: String!): EmailSignUpResponse!\n  AuthFacebook(firstName: String!, lastName: String!, email: String, facebookId: String!): AuthFacebookResponse!\n  SendChatMessage(text: String!, chatId: Int!): SendChatMessageResponse!\n  CreatePlace(name: String!, lat: Float!, lng: Float!, address: String!, isFavorite: Boolean!): CreatePlaceResponse!\n  DeletePlace(id: Int!): DeletePlaceResponse!\n  EditPlace(id: Int!, name: String, isFavorite: Boolean): EditPlaceResponse!\n  RequestRide(pickUpAddress: String!, pickUpLat: Float!, pickUpLng: Float!, dropOffAddress: String!, dropOffLat: Float!, dropOffLng: Float!, price: Float!, distance: String!, duration: String!): RequestRideResponse!\n  UpdateRideStatus(rideId: Int!, status: Statuses!): UpdateRideStatusResponse!\n  UpdateUserAccountData(firstName: String, lastName: String, email: String, pass: String, age: Int): UpdateUserAccountDataResponse!\n  ChangeDrivingMode: ChangeDrivingModeResponse!\n  MovementStatus(lastOrientation: Float, lastLat: Float, lastLng: Float): MovementStatusResponse!\n  PhoneVerification(phoneNumber: String!): PhoneVerificationResponse!\n  ValidateVerificationCode(phoneNumber: String!, code: String!): ValidateVerificationCodeResponse!\n}\n\ntype EmailSignUpResponse {\n  ok: Boolean!\n  error: String\n  token: String\n}\n\ntype AuthFacebookResponse {\n  ok: Boolean!\n  error: String\n  token: String\n}\n\ntype GetChatResponse {\n  ok: Boolean!\n  error: String\n  chat: Chat\n}\n\ntype Query {\n  GetChat(chatId: Int!): GetChatResponse!\n  GetAllPlaces: GetAllPlacesResponse!\n  GetNearestRide: GetNearestRideResponse!\n  GetRide(rideId: Int!): GetRideResponse!\n  user: User\n  GetUserAccountData: GetUserAccountDataResponse!\n  GetNearestDrivers: GetNearestDriversResponse!\n}\n\ntype Subscription {\n  MessageSubscription: Message\n  RideStatusSubscription: Ride\n  SubscribeOnNearestRide: Ride\n  SubscribeOnNearestDriver: User\n}\n\ntype SendChatMessageResponse {\n  ok: Boolean!\n  error: String\n  message: Message\n}\n\ntype Chat {\n  id: Int!\n  messages: [Message]\n  passengerId: Int!\n  passenger: User!\n  driverId: Int\n  driver: User!\n  rideId: Int\n  ride: Ride!\n  createdAt: String!\n  updatedAt: String\n}\n\ntype Message {\n  id: Int!\n  text: String!\n  chatId: Int\n  chat: Chat!\n  user: User!\n  createdAt: String!\n  updatedAt: String\n}\n\ntype CreatePlaceResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype DeletePlaceResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype EditPlaceResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype GetAllPlacesResponse {\n  ok: Boolean!\n  error: String\n  places: [Place]\n}\n\ntype Place {\n  id: Int!\n  name: String!\n  lat: Float!\n  lng: Float!\n  address: String!\n  isFavorite: Boolean!\n  userId: Int!\n  user: User!\n  createdAt: String!\n  updatedAt: String\n}\n\ntype GetNearestRideResponse {\n  ok: Boolean!\n  error: String\n  ride: Ride\n}\n\ntype GetRideResponse {\n  ok: Boolean!\n  error: String\n  ride: Ride\n}\n\ntype RequestRideResponse {\n  ok: Boolean!\n  error: String\n  ride: Ride\n}\n\ntype Ride {\n  id: Int!\n  status: String!\n  pickUpAddress: String!\n  pickUpLat: Float!\n  pickUpLng: Float!\n  dropOffAddress: String!\n  dropOffLat: Float!\n  dropOffLng: Float!\n  price: Float!\n  distance: String!\n  duration: String!\n  driverId: Int!\n  driver: User!\n  passengerId: Int!\n  passenger: User!\n  chatId: Int\n  chat: Chat\n  createdAt: String!\n  updatedAt: String\n}\n\nenum Statuses {\n  ACCEPTED\n  FINISHED\n  CANCELED\n  REQUESTING\n  ONROUTE\n}\n\ntype UpdateRideStatusResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype User {\n  id: Int!\n  email: String\n  verifiedEmail: Boolean!\n  firstName: String!\n  lastName: String!\n  bio: String\n  age: Int\n  pass: String\n  phoneNumber: String\n  verifiedPhoneNumber: Boolean!\n  facebookId: String!\n  isDriving: Boolean!\n  isRiding: Boolean!\n  isTaken: Boolean!\n  lastLng: Float\n  lastLat: Float\n  lastOrientation: Float\n  fullName: String\n  messages: [Message]\n  ridesAsPassenger: [Ride]\n  ridesAsDriver: [Ride]\n  places: [Place]\n  createdAt: String!\n  updatedAt: String\n}\n\ntype GetUserAccountDataResponse {\n  ok: Boolean!\n  error: String\n  user: User\n}\n\ntype UpdateUserAccountDataResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype ChangeDrivingModeResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype GetNearestDriversResponse {\n  ok: Boolean!\n  error: String\n  drivers: [User]\n}\n\ntype MovementStatusResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype PhoneVerificationResponse {\n  ok: Boolean!\n  error: String\n}\n\ntype Verification {\n  id: Int!\n  target: String!\n  payload: String!\n  code: String!\n  verified: Boolean!\n  createdAt: String!\n  updatedAt: String\n}\n\ntype ValidateVerificationCodeResponse {\n  ok: Boolean!\n  error: String\n  token: String\n}\n"];
/* tslint:disable */

export interface Query {
  GetChat: GetChatResponse;
  GetAllPlaces: GetAllPlacesResponse;
  GetNearestRide: GetNearestRideResponse;
  GetRide: GetRideResponse;
  user: User | null;
  GetUserAccountData: GetUserAccountDataResponse;
  GetNearestDrivers: GetNearestDriversResponse;
}

export interface GetChatQueryArgs {
  chatId: number;
}

export interface GetRideQueryArgs {
  rideId: number;
}

export interface GetChatResponse {
  ok: boolean;
  error: string | null;
  chat: Chat | null;
}

export interface Chat {
  id: number;
  messages: Array<Message> | null;
  passengerId: number;
  passenger: User;
  driverId: number | null;
  driver: User;
  rideId: number | null;
  ride: Ride;
  createdAt: string;
  updatedAt: string | null;
}

export interface Message {
  id: number;
  text: string;
  chatId: number | null;
  chat: Chat;
  user: User;
  createdAt: string;
  updatedAt: string | null;
}

export interface User {
  id: number;
  email: string | null;
  verifiedEmail: boolean;
  firstName: string;
  lastName: string;
  bio: string | null;
  age: number | null;
  pass: string | null;
  phoneNumber: string | null;
  verifiedPhoneNumber: boolean;
  facebookId: string;
  isDriving: boolean;
  isRiding: boolean;
  isTaken: boolean;
  lastLng: number | null;
  lastLat: number | null;
  lastOrientation: number | null;
  fullName: string | null;
  messages: Array<Message> | null;
  ridesAsPassenger: Array<Ride> | null;
  ridesAsDriver: Array<Ride> | null;
  places: Array<Place> | null;
  createdAt: string;
  updatedAt: string | null;
}

export interface Ride {
  id: number;
  status: string;
  pickUpAddress: string;
  pickUpLat: number;
  pickUpLng: number;
  dropOffAddress: string;
  dropOffLat: number;
  dropOffLng: number;
  price: number;
  distance: string;
  duration: string;
  driverId: number;
  driver: User;
  passengerId: number;
  passenger: User;
  chatId: number | null;
  chat: Chat | null;
  createdAt: string;
  updatedAt: string | null;
}

export interface Place {
  id: number;
  name: string;
  lat: number;
  lng: number;
  address: string;
  isFavorite: boolean;
  userId: number;
  user: User;
  createdAt: string;
  updatedAt: string | null;
}

export interface GetAllPlacesResponse {
  ok: boolean;
  error: string | null;
  places: Array<Place> | null;
}

export interface GetNearestRideResponse {
  ok: boolean;
  error: string | null;
  ride: Ride | null;
}

export interface GetRideResponse {
  ok: boolean;
  error: string | null;
  ride: Ride | null;
}

export interface GetUserAccountDataResponse {
  ok: boolean;
  error: string | null;
  user: User | null;
}

export interface GetNearestDriversResponse {
  ok: boolean;
  error: string | null;
  drivers: Array<User> | null;
}

export interface Mutation {
  EmailSignIn: EmailSignInResponse;
  EmailSignUp: EmailSignUpResponse;
  AuthFacebook: AuthFacebookResponse;
  SendChatMessage: SendChatMessageResponse;
  CreatePlace: CreatePlaceResponse;
  DeletePlace: DeletePlaceResponse;
  EditPlace: EditPlaceResponse;
  RequestRide: RequestRideResponse;
  UpdateRideStatus: UpdateRideStatusResponse;
  UpdateUserAccountData: UpdateUserAccountDataResponse;
  ChangeDrivingMode: ChangeDrivingModeResponse;
  MovementStatus: MovementStatusResponse;
  PhoneVerification: PhoneVerificationResponse;
  ValidateVerificationCode: ValidateVerificationCodeResponse;
}

export interface EmailSignInMutationArgs {
  email: string;
  pass: string;
}

export interface EmailSignUpMutationArgs {
  firstName: string;
  lastName: string;
  email: string | null;
  pass: string;
  age: number;
  phoneNumber: string;
}

export interface AuthFacebookMutationArgs {
  firstName: string;
  lastName: string;
  email: string | null;
  facebookId: string;
}

export interface SendChatMessageMutationArgs {
  text: string;
  chatId: number;
}

export interface CreatePlaceMutationArgs {
  name: string;
  lat: number;
  lng: number;
  address: string;
  isFavorite: boolean;
}

export interface DeletePlaceMutationArgs {
  id: number;
}

export interface EditPlaceMutationArgs {
  id: number;
  name: string | null;
  isFavorite: boolean | null;
}

export interface RequestRideMutationArgs {
  pickUpAddress: string;
  pickUpLat: number;
  pickUpLng: number;
  dropOffAddress: string;
  dropOffLat: number;
  dropOffLng: number;
  price: number;
  distance: string;
  duration: string;
}

export interface UpdateRideStatusMutationArgs {
  rideId: number;
  status: Statuses;
}

export interface UpdateUserAccountDataMutationArgs {
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  pass: string | null;
  age: number | null;
}

export interface MovementStatusMutationArgs {
  lastOrientation: number | null;
  lastLat: number | null;
  lastLng: number | null;
}

export interface PhoneVerificationMutationArgs {
  phoneNumber: string;
}

export interface ValidateVerificationCodeMutationArgs {
  phoneNumber: string;
  code: string;
}

export interface EmailSignInResponse {
  ok: boolean;
  error: string | null;
  token: string | null;
}

export interface EmailSignUpResponse {
  ok: boolean;
  error: string | null;
  token: string | null;
}

export interface AuthFacebookResponse {
  ok: boolean;
  error: string | null;
  token: string | null;
}

export interface SendChatMessageResponse {
  ok: boolean;
  error: string | null;
  message: Message | null;
}

export interface CreatePlaceResponse {
  ok: boolean;
  error: string | null;
}

export interface DeletePlaceResponse {
  ok: boolean;
  error: string | null;
}

export interface EditPlaceResponse {
  ok: boolean;
  error: string | null;
}

export interface RequestRideResponse {
  ok: boolean;
  error: string | null;
  ride: Ride | null;
}

export type Statuses = "ACCEPTED" | "FINISHED" | "CANCELED" | "REQUESTING" | "ONROUTE";

export interface UpdateRideStatusResponse {
  ok: boolean;
  error: string | null;
}

export interface UpdateUserAccountDataResponse {
  ok: boolean;
  error: string | null;
}

export interface ChangeDrivingModeResponse {
  ok: boolean;
  error: string | null;
}

export interface MovementStatusResponse {
  ok: boolean;
  error: string | null;
}

export interface PhoneVerificationResponse {
  ok: boolean;
  error: string | null;
}

export interface ValidateVerificationCodeResponse {
  ok: boolean;
  error: string | null;
  token: string | null;
}

export interface Subscription {
  MessageSubscription: Message | null;
  RideStatusSubscription: Ride | null;
  SubscribeOnNearestRide: Ride | null;
  SubscribeOnNearestDriver: User | null;
}

export interface Verification {
  id: number;
  target: string;
  payload: string;
  code: string;
  verified: boolean;
  createdAt: string;
  updatedAt: string | null;
}

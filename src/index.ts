import { config } from "dotenv"; config();
import { Options } from "graphql-yoga";
import { createConnection } from "typeorm";

import app from "./app";
import dbConnectionsOptions from "./dbConfig";
import jwtDecoder from "./utils/jwtDecoder";

const PORT: number | string = process.env.PORT || 4000;
const PLAYGROUND_ENDPOINT: string = "/playground";
const GRAPHQL_ENDPOINT: string = "/graphql";
const SUBSCRIPTIONS_ENDPOINT: string = "/subscriptions"

const appOptions: Options = {
    port: PORT,
    playground: PLAYGROUND_ENDPOINT,
    endpoint: GRAPHQL_ENDPOINT,
    subscriptions: {
        path: SUBSCRIPTIONS_ENDPOINT,
        onConnect: async connectionsParams => {
            const token = connectionsParams['X-JWT'];
            if (token) {
                const user = await jwtDecoder(token);
                if (user) {
                    return {
                        currentUser: user
                    }
                }
            }

            throw new Error('No token')
        }
    }
};

createConnection(dbConnectionsOptions).then(() => {
    app.start(appOptions, () => console.log(`Server is running on localhost:${PORT}`));
}).catch(error => console.log(error));



import { ConnectionOptions } from "typeorm";
import User from './entities/User';
import Verification from "./entities/Verification";
import Place from "./entities/Place";
import Ride from "./entities/Ride";
import Chat from "./entities/Chat";
import Message from "./entities/Message";

const dbConnectionsOptions: ConnectionOptions = {
    type: "mysql",
    host: process.env.TYPEORM_ENDPOINT,
    database: process.env.TYPEORM_DB_NAME,
    port: parseInt(process.env.TYPEORM_PORT) || 3306,
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    entities: [
        User,
        Verification,
        Place,
        Ride,
        Chat,
        Message
    ],
    synchronize: true,
    logging: ["error"]
}

export default dbConnectionsOptions;

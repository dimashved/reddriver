import { Resolvers } from "../../../types/resolvers";
import { ValidateVerificationCodeMutationArgs, ValidateVerificationCodeResponse } from "../../../types/graph";
import Verification from "../../../entities/Verification";
import User from "../../../entities/User";
import createJWT from "../../../utils/jwt";

const resolvers: Resolvers = {
    Mutation: {
        ValidateVerificationCode: async(_, args: ValidateVerificationCodeMutationArgs): Promise<ValidateVerificationCodeResponse> => {
            const { phoneNumber, code } = args;

            try {
                const verification = await Verification.findOne({
                    payload: phoneNumber,
                    code
                });

                if (!verification) {
                    return {
                        ok: false,
                        error: "Verification code not valid",
                        token: null
                    };
                } else {
                    verification.verified = true;
                    await verification.save();
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message,
                    token: null
                };
            }

            try {
                const user = await User.findOne({ phoneNumber });

                if (user) {
                    user.verifiedPhoneNumber = true;
                    await user.save();

                    const token = createJWT(user.id);

                    return {
                        ok: true,
                        error: null,
                        token
                    };
                } else {
                    return {
                        ok: true,
                        error: null,
                        token: null
                    };
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message,
                    token: null
                };
            }
        }
    }
}

export default resolvers;

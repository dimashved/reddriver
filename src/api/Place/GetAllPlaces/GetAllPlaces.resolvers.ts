import { Resolvers } from "../../../types/resolvers";
import authResolver from "../../../utils/authResolver";
import { GetAllPlacesResponse } from "../../../types/graph";
import User from "../../../entities/User";

const resolvers: Resolvers = {
    Query: {
        GetAllPlaces: authResolver(async(_, __, { req }): Promise<GetAllPlacesResponse> => {
            try {
                const user = await User.findOne({ id: req.user.id }, { relations: ["places"] });

                if (user) {
                    return {
                        ok: true,
                        error: null,
                        places: user.places
                    };
                } else {
                    return {
                        ok: false,
                        error: `User not found`,
                        places: null
                    };
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message,
                    places: null
                };
            }
        })
    }
};

export default resolvers;

import { Resolvers } from "../../../types/resolvers";
import authResolver from "../../../utils/authResolver";
import User from "../../../entities/User";
import Place from "../../../entities/Place";
import { EditPlaceMutationArgs, EditPlaceResponse } from "../../../types/graph";
import filterNullArgs from "../../../utils/filterNullArgs";

const resolvers: Resolvers = {
    Mutation: {
        EditPlace: authResolver(async(_, args: EditPlaceMutationArgs, { req }): Promise<EditPlaceResponse> => {
            const user: User = req.user;

            try {
                const place = await Place.findOne({ id: args.id });

                if (place) {
                    if (place.userId === user.id) {
                        const notNull = filterNullArgs(args);
                        await Place.update({ id: args.id }, { ...notNull });

                        return {
                            ok: true,
                            error: null
                        };
                    } else {
                        return {
                            ok: false,
                            error: "Not authorized"
                        };
                    }
                } else {
                    return {
                        ok: false,
                        error: "Place not found"
                    }
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message
                }
            }
        })
    }
}

export default resolvers;

import { Resolvers } from "../../../types/resolvers";
import authResolver from "../../../utils/authResolver";
import User from "../../../entities/User";
import Place from "../../../entities/Place";
import { CreatePlaceMutationArgs, CreatePlaceResponse } from "../../../types/graph";

const resolvers: Resolvers = {
    Mutation: {
        CreatePlace: authResolver(async(_, args: CreatePlaceMutationArgs, { req }): Promise<CreatePlaceResponse> => {
            const user: User = req.user;

            try {
                await Place.create({ ...args, user }).save();

                return {
                    ok: true,
                    error: null
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message
                }
            }
        })
    }
}

export default resolvers;

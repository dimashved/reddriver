import { Resolvers } from "../../../../types/resolvers";
import authResolver from "../../../../utils/authResolver";
import User from "../../../../entities/User";
import { GetNearestDriversResponse } from "../../../../types/graph";
import { Between, getRepository } from "typeorm";

const resolvers: Resolvers = {
    Query: {
        GetNearestDrivers: authResolver(async(_, __, { req }): Promise<GetNearestDriversResponse> => {
            const user: User = req.user;
            const { lastLat, lastLng } = user;

            try {
                const drivers: User[] = await getRepository(User).find({
                    isDriving: true,
                    lastLat: Between(lastLat - 0.05, lastLat + 0.05),
                    lastLng: Between(lastLng - 0.05, lastLng + 0.05)
                });

                return {
                    ok: true,
                    error: null,
                    drivers
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message,
                    drivers: null
                }
            }
        })
    }
};

export default resolvers;

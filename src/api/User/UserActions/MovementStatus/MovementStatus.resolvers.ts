import { Resolvers } from "../../../../types/resolvers";
import authResolver from "../../../../utils/authResolver";
import { MovementStatusMutationArgs, MovementStatusResponse } from "../../../../types/graph";
import User from "../../../../entities/User";
import filterNullArgs from "../../../../utils/filterNullArgs";

const resolvers: Resolvers = {
    Mutation: {
        MovementStatus: authResolver(async(_, args: MovementStatusMutationArgs, { req, pubSub }): Promise<MovementStatusResponse> => {
            const user: User = req.user;
            const notNull = filterNullArgs(args);

            try {
                await User.update({ id: user.id }, { ...notNull });
                const updatedUser = await User.findOne({ id: user.id });
                pubSub.publish('driverUpdate', { DriverSubscription: updatedUser });

                return {
                    ok: true,
                    error: null
                };
            } catch (error) {
                return {
                    ok: false,
                    error: error.message
                };
            }
        })
    }
};

export default resolvers;

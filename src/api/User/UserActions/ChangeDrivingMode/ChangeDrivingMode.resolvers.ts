import { Resolvers } from "../../../../types/resolvers";
import authResolver from "../../../../utils/authResolver";
import { ChangeDrivingModeResponse } from "../../../../types/graph";
import User from "../../../../entities/User";

const resolvers: Resolvers = {
    Mutation: {
        ChangeDrivingMode: authResolver(async(_, __, { req }): Promise<ChangeDrivingModeResponse> => {
            const user: User = req.user;

            user.isDriving = !user.isDriving;
            await user.save();

            return {
                ok: true, error: null
            }
        })
    }
}

export default resolvers;

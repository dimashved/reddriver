import authResolver from "../../../../utils/authResolver";
import { UpdateUserAccountDataMutationArgs, UpdateUserAccountDataResponse } from "../../../../types/graph";
import User from "../../../../entities/User";
import { Resolvers } from "../../../../types/resolvers";
import filterNullArgs from "../../../../utils/filterNullArgs";

const resolvers: Resolvers = {
    Mutation: {
        UpdateUserAccountData: authResolver(async(_, args: UpdateUserAccountDataMutationArgs, { req }): Promise<UpdateUserAccountDataResponse> => {
            const user: User = req.user;
            // Need if value will be null
            const notNull = filterNullArgs(args);

            try {
                if (args.pass !== null) {
                    user.pass = args.pass;
                    await user.save();
                }

                await User.update({id: user.id}, { ...notNull });

                return {
                    ok: true,
                    error: null
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message
                }
            }
        })
    }
};

export default resolvers;

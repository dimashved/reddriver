import { Resolvers } from "../../../types/resolvers";
import { AuthFacebookMutationArgs, AuthFacebookResponse } from "../../../types/graph";
import User from "../../../entities/User";
import createJWT from "../../../utils/jwt";

const resolvers: Resolvers = {
    Mutation: {
        AuthFacebook: async (_, args: AuthFacebookMutationArgs): Promise<AuthFacebookResponse> => {
            const { facebookId } = args

            try {
                const user = await User.findOne({ facebookId })

                if (user) {
                    const token = createJWT(user.id);

                    return {
                        ok: true,
                        error: null,
                        token
                    };
                }
            } catch (e) {
                return {ok: false, error: e.message, token: null}
            }

            try {
                const createUser = await User.create({...args}).save();
                const token = createJWT(createUser.id);

                return {ok: true, error: null, token}
            } catch (e) {
                return {ok: false, error: e.message, token: null}
            }
        }
    }
}

export default resolvers;

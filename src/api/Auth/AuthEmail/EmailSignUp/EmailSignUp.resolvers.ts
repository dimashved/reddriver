import { Resolvers } from "../../../../types/resolvers";
import { EmailSignUpMutationArgs, EmailSignUpResponse } from "../../../../types/graph";
import User from "../../../../entities/User";
import createJWT from "../../../../utils/jwt";

const resolvers: Resolvers = {
    Mutation: {
        EmailSignUp: async (_, args: EmailSignUpMutationArgs): Promise<EmailSignUpResponse> => {
            const { email } = args;

            try {
                const user = await User.findOne({ email });

                if (user) {
                    return {
                        ok: true,
                        error: "Please, login first",
                        token: null
                    };
                } else {
                    const newUser = await User.create({ ...args }).save();

                    const token = createJWT(newUser.id);

                    return {
                        ok: true,
                        error: null,
                        token
                    }
                }
            } catch (error) {
                return {
                    ok: false,
                    error: error.message,
                    token: null
                }
            }
        }
    }
}

export default resolvers;

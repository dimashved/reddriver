import { Resolvers } from "../../../../types/resolvers";
import { EmailSignInMutationArgs, EmailSignInResponse } from "../../../../types/graph";
import User from "../../../../entities/User";
import createJWT from "../../../../utils/jwt";

const resolvers: Resolvers = {
    Mutation: {
        EmailSignIn: async (_, args: EmailSignInMutationArgs): Promise<EmailSignInResponse> => {
            const { email, pass } = args;

            try {
                const user = await User.findOne({ email });

                if (!user) {
                    return {
                        ok: false,
                        error: "User with this email doesn't exist",
                        token: null
                    };
                }

                const checkPassword = await user.passwordsComparing(pass);
                if (checkPassword) {
                    const token = createJWT(user.id);
                    return {
                        ok: true,
                        error: null,
                        token
                    };
                } else {
                    return {
                        ok: false,
                        error: "Wong password",
                        token: null
                    };
                }
            } catch (error) {
                return  {
                    ok: false,
                    error: error.message,
                    token: null
                }
            }
        }
    }
};

export default resolvers;

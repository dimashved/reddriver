import { Resolvers } from "../../../types/resolvers";
import authResolver from "../../../utils/authResolver";
import { UpdateRideStatusMutationArgs, UpdateRideStatusResponse } from "../../../types/graph";
import User from "../../../entities/User";
import Ride from "../../../entities/Ride";
import Chat from "../../../entities/Chat";

const resolvers: Resolvers = {
    Mutation: {
        UpdateRideStatus: authResolver(async (_, args: UpdateRideStatusMutationArgs, { req, pubSub }): Promise<UpdateRideStatusResponse> => {
            const user: User = req.user;

            if (user.isDriving) {
                try {
                    let ride: Ride | undefined;

                    if (args.status === "ACCEPTED") {
                        ride = await Ride.findOne({
                                id: args.rideId,
                                status: "REQUESTING"
                            },
                            { relations: ["passenger"] }
                        );

                        if (ride) {
                            ride.driver = user;
                            user.isTaken = true;
                            await user.save();

                            ride.chat = await Chat.create({
                                driver: user,
                                passenger: ride.passenger
                            }).save();

                            await ride.save()
                        }
                    } else {
                        ride = await Ride.findOne({
                            id: args.rideId,
                            driver: user
                        });
                    }

                    if (ride) {
                        ride.status = args.status;
                        await ride.save();
                        pubSub.publish("rideUpdate", { RideStatusSubscription: ride });

                        return {
                            ok: true,
                            error: null
                        };
                    } else {
                        return {
                            ok: false,
                            error: "You cant update ride"
                        };
                    }
                } catch (error) {
                    return {
                        ok: false,
                        error: error.message
                    };
                }
            } else {
                return {
                    ok: false,
                    error: "You are not driving"
                };
            }
        })
    }
}

export default resolvers;

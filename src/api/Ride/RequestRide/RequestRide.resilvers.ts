import { Resolvers } from "../../../types/resolvers";
import authResolver from "../../../utils/authResolver";
import { RequestRideMutationArgs, RequestRideResponse } from "../../../types/graph";
import User from "../../../entities/User";
import Ride from "../../../entities/Ride";

const resolvers: Resolvers = {
    Mutation: {
        RequestRide: authResolver(async(_, args: RequestRideMutationArgs, { req, pubSub }): Promise<RequestRideResponse> => {
            const user: User = req.user;

            if (!user.isRiding && !user.isDriving) {
                try {
                    const ride = await Ride.create({ ...args, passenger: user }).save();
                    pubSub.publish('rideRequest', { SubscribeOnNearestRide: ride });
                    user.isRiding = true;
                    await user.save();

                    return {
                        ok: true,
                        error: null,
                        ride
                    };
                } catch (error) {
                    return {
                        ok: false,
                        error: error.message,
                        ride: null
                    };
                }
            } else {
                return {
                    ok: false,
                    error: "You can't request a ride",
                    ride: null
                };
            }
        })
    }
}

export default resolvers;

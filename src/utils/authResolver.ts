const authResolver = functionResolver => async (parent, args, context, info) => {
    if (!context.req.user) {
        throw new Error('JWT not exist');
    }

    return await functionResolver(parent, args, context, info);
};

export default authResolver;

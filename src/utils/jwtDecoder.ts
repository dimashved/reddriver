import { verify } from "jsonwebtoken";
import User from "../entities/User";

const jwtDecoder = async (token: string): Promise<User | undefined> => {
    try {
        const decoded: any = verify(token, process.env.JWT_TOKEN || "");
        const { id } = decoded;

        return await User.findOne({ id });
    } catch (error) {
        return error;
    }
};

export default jwtDecoder;

import { sign } from "jsonwebtoken"

const createJWT = (id: number): string => {
    return sign({
        id
    }, process.env.JWT_TOKEN)
};

export default createJWT;

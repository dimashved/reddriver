import * as Twilio from "twilio";

const client = Twilio(
    process.env.TWILIO_ACC_SID,
    process.env.TWILIO_TOKEN
);

export const sendSMS = (to: string, body: string): Promise<any> => {
    return client.messages.create({
        body,
        messagingServiceSid: process.env.TWILIO_SID,
        to
    });
}

export const sendVerificationSMS = (to: string, key: string) => sendSMS(to, key);

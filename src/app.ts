import { GraphQLServer, PubSub } from "graphql-yoga";
import { NextFunction, Response } from "express";

import * as cors from "cors";
import * as helmet from "helmet";
import * as logger from "morgan";

import schema from "./schema"
import jwtDecoder from "./utils/jwtDecoder";

class App {
    public app: GraphQLServer;

    public pubSub: any;

    constructor() {
        this.pubSub = new PubSub();
        this.pubSub.ee.setMaxListeners(99);
        this.app = new GraphQLServer({
            schema: schema,
            context: req => {
                const { connection: { context = null } = {} } = req
                return {
                    req: req.request,
                    pubSub: this.pubSub,
                    context: context
                }
            }
        });
        this.middlewares();
    }

    private middlewares = (): void => {
        this.app.express.use(cors());
        this.app.express.use(logger("dev"));
        this.app.express.use(this.jwt);
    }

    private jwt = async(req, res: Response, next: NextFunction): Promise<void> => {
        const token = req.get("X-JWT");

        if (token) {
            const user = await jwtDecoder(token);
            if (user) {
                req.user = user;
            } else {
                req.user = undefined;
            }
        }
        next();
    }
}

export default new App().app;

# RedDriver
Simplified implementation of an application for providing transportation services for people

## Installation
```bash
nvm use 16.14.2

npm install

# add your credentionals for db connections | or edin in ./src/dbConfig.ts
cp ./src/.env.sample ./src/.env
```

docker-compose

```shell
docker-compose up -d
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
